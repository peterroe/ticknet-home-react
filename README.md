## description: 招新网页
## anthor: 林舒恒
## LastUpdate: 林舒恒
## LastUpdateTime:2021.7.8

## 运行：
``` shell
yarn start
```

## 打包：
```shell
yarn build
```

## 注意事项
 + 相比于Vue的版本，因为React没有提供类似Vue的v-mode指令，所以成员处无动态过渡效果
 + 因为是基于Vue重构，结构看起来比初版Vue好一些

 ## 项目地址
 ```shell
https://gitlab.com/peterroe/ticknet-home-react
 ```