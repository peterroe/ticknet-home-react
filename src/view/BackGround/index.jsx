import React from 'react'
import './index.css'
import {Button} from 'antd'

export default class Background extends React.Component{
    render() {
        return (
            <div className="bg-one">
            <div className="bg-content">
                <div className="bg-title">Tick Net工作室</div>
                <div className="bg-p">
                    不断成长不断进步 锐意进取的优秀团队<br />等待您的加入
                </div>
                <a href="http://join.ticknet.hnust.cn"><Button size="large" className="joinWe"> 加入我们 </Button></a>
            </div>
        </div>
        )
    }
}