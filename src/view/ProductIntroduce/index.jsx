import React from "react";
import './index.css'
import { addClass, removeClass } from '../../util/oprateClass.js'
export default class ProductIntroduce extends React.Component {
    constructor(props) {
        super(props)
        
    }
    componentDidMount() {
        if (document.body.clientWidth < 768) {
            return
        }
        const ps = document.querySelectorAll('.p-content')
        for (let i = 0; i < ps.length; i++) {
            ps[i].addEventListener('mouseenter', () => {
                addClass(ps[i], 'swing')
            })
            ps[i].addEventListener('mouseleave', () => {
                removeClass(ps[i], 'swing')
            })
        }
    }
    render() {
        return (
            <div id="product" className="product container">
                <div className="product-title">产品介绍</div>
                <div className="product-contents">
                    <div className="p-content animated">
                        <div className="p-content-icon">⚑</div>
                        <div className="p-content-title">学习服务</div>
                        <div className="p-content-p">
                            查课表、查成绩、查教室、图书查询、图书续借
                        </div>
                    </div>
                    <div className="p-content animated">
                        <div className="p-content-icon">⚑</div>
                        <div className="p-content-title">面对面签到</div>
                        <div className="p-content-p">基于位置的签到系统</div>
                    </div>
                    <div className="p-content animated">
                        <div className="p-content-icon">⚑</div>
                        <div className="p-content-title">微课堂</div>
                        <div className="p-content-p">优秀的教学辅助工具</div>
                    </div>
                    <div className="p-content animated">
                        <div className="p-content-icon">⚑</div>
                        <div className="p-content-title">失物招领</div>
                        <div className="p-content-p">亲，丢东西了吗？</div>
                    </div>
                    <div className="p-content animated">
                        <div className="p-content-icon">⚑</div>
                        <div className="p-content-title">上网注册</div>
                        <div className="p-content-p">助力校友临时上网</div>
                    </div>
                    <div className="p-content animated">
                        <div className="p-content-icon">⚑</div>
                        <div className="p-content-title">校内社区</div>
                        <div className="p-content-p">科大人自己的社区</div>
                    </div>
                    <div className="p-content animated">
                        <div className="p-content-icon">⚑</div>
                        <div className="p-content-title">党校系统</div>
                        <div className="p-content-p">想要为入党的你提供一个便捷的平台</div>
                    </div>
                    <div className="p-content animated">
                        <div className="p-content-icon">⚑</div>
                        <div className="p-content-title">校园闲置</div>
                        <div className="p-content-p">同校当面交易，物尽其用！</div>
                    </div>
                    <div className="p-content animated">
                        <div className="p-content-icon">⚑</div>
                        <div className="p-content-title">汽车入校申请</div>
                        <div className="p-content-p">科大人乘车回校必备</div>
                    </div>
                    <div className="p-content animated">
                        <div className="p-content-icon">⚑</div>
                        <div className="p-content-title">微信考勤系统</div>
                        <div className="p-content-p">艺术学院的考勤神器</div>
                    </div>
                </div>
            </div>
        )
    }
}