import React from "react";
import './index.css'
import { Row,Col } from "antd";
import { CSSTransition, TransitionGroup } from 'react-transition-group'
export default class MemberIntruduce extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            $timeout: null,
            imgs : [
                {
                    imgsrc: 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=1004953359,2131419137&fm=11&gp=0.jpg',
                    row: '1',
                    col: '1',
                    no: 'kjl',
                    name: '杰罗姆',
                    p: '不要因为没有掌声就放弃梦想'
                },
                {
                    imgsrc: 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3155998395,3600507640&fm=26&gp=0.jpg',
                    row: '1',
                    col: '2',
                    no: 'yui',
                    name: '伊迪斯',
                    p: '但愿日子清静，抬头遇见的都是柔情'
                },
                {
                    imgsrc: 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=1709216491,2536617744&fm=26&gp=0.jpg',
                    row: '1',
                    col: '3',
                    no: 'fhj',
                    name: '阿尔瓦',
                    p: '对于值得与热爱的事情，就要做到极致'
                },
                {
                    imgsrc: 'https://img2.baidu.com/it/u=325567737,3478266281&fm=26&fmt=auto&gp=0.jpg',
                    row: '1',
                    col: '4',
                    no: 'ddd',
                    name: '埃琳娜',
                    p: '你什么都不知道，包括我喜欢你'
                },
                {
                    imgsrc: 'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1475331839,2066156315&fm=26&gp=0.jpg',
                    row: '1',
                    col: '5',
                    no: 'dfg',
                    name: '亚伯拉罕',
                    p: '活得太清醒本就是件不浪漫的事'
                },
                {
                    imgsrc: 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=1545190233,2172456233&fm=26&gp=0.jpg',
                    row: '2',
                    col: '2',
                    no: 'sfgfh',
                    name: '伊斯特尔',
                    p: '火苗再小，也可以反复点燃'
                },
                {
                    imgsrc: 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=3641306367,2787304394&fm=26&gp=0.jpg',
                    row: '2',
                    col: '3',
                    no: 'ghj',
                    name: '安东尼',
                    p: '总要尝遍所有的路，在对生活充满期待'
                },
                {
                    imgsrc: 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2013128738,1744245349&fm=26&gp=0.jpg',
                    row: '2',
                    col: '4',
                    no: 'asfgfg',
                    name: '洛福斯',
                    p: '从此天光大亮，你是我全部的渴望与幻想'
                },
    
                {
                    imgsrc: 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3222454269,3484359568&fm=26&gp=0.jpg',
                    row: '2',
                    col: '5',
                    no: 'ktyu',
                    name: '埃尔维斯',
                    p: '所有的温柔都源于你的强大'
                },
                {
                    imgsrc: 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2622623191,3152180496&fm=26&gp=0.jpg',
                    row: '3',
                    col: '2',
                    no: 'yjkh',
                    name: '埃德温',
                    p: '努力让自己发光，对的人才会迎光而来'
                },
                {
                    imgsrc: 'https://img0.baidu.com/it/u=1001760444,1818781497&fm=26&fmt=auto&gp=0.jpg',
                    row: '3',
                    col: '3',
                    no: 'ghkn',
                    name: '格伦',
                    p: '记得在这杂乱的生活里，每天带点笑意'
                },
                {
                    imgsrc: 'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1709644436,2739756540&fm=26&gp=0.jpg',
                    row: '3',
                    col: '4',
                    no: 'hjkty',
                    name: '伊迪斯',
                    p: '有人喝了酒眼睛亮闪闪的讲浪漫和爱'
                },
                {
                    imgsrc: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=2100401123,2895311668&fm=26&gp=0.jpg',
                    row: '5',
                    col: '3',
                    no: 'htj',
                    name: '沃伦',
                    p: '你的心，是我想要到达的蔚蓝海岸'
                }
            ]
        }
        this.shut = this.shut.bind(this)
        this.shuffle = this.shuffle.bind(this)
    }
    shut(e) {
        // console.log(e)
        clearInterval(this.state.$timeout)
        let temp = Array.from(this.state.imgs)
        temp.unshift(...temp.splice(e, 1))
        this.setState({
            imgs: temp
        })
        this.state.$timeout = setInterval(() => {
            this.shuffle()
        }, 3000)
    }
    shuffle() {
        let temp = Array.from(this.state.imgs)
        temp.unshift(temp.pop())
        this.setState({
            imgs:temp
        })
        // this.state.imgs.unshift(this.state.imgs.pop())
    }
    componentDidMount() {
        this.setState({
            $timeout : setInterval(() => {
                this.shuffle()
            }, 3000)
        })  
    }
    
componentWillUnMount = () => {
    clearInterval(this.state.$timeout)
    // this.setState({
    //     $timeout : null
    // }) 
}
    render() {
        
        return (
            <div className="demo container">
                <Row>
                    <Col span="12">
                        {
                            this.state.imgs[0] && (
                                <div className="showMsg">
                                <img src={this.state.imgs[0].imgsrc} />
                            </div>)
                        }

                    </Col>
                    <Col span="12">
                        <div className="a-content">
                            <div className="a-content-title">
                                { this.state.imgs[0] ? this.state.imgs[0].name : '' }
                            </div>
                            <div className="a-content-p">
                                { this.state.imgs[0] ? this.state.imgs[0].p : '' }
                            </div>
                        </div>
                    </Col>
                </Row>
                {/* 很遗憾没有过渡效果 */}
                <TransitionGroup
                    className="a-container"
                >
                    {
                        this.state.imgs.map((item,index) => {
                            return (
                                <CSSTransition
                                    timeout={1000}
                                    key={item.imgsrc}
                                    classNames="show"
                                    unmountOnExit
                                >
                                    <div className="cell" onClick={()=>this.shut(index)}>
                                        <img src={item.imgsrc} alt="" />
                                    </div>
                                    
                                </CSSTransition>
                            )
                        })
                    }
                </TransitionGroup>
        {/* <transition-group name="cells" tag="div" class="a-container">
            <div
                v-for="(img, index) in imgs"
                :key="img.no"
                class="cell"
                @click.capture.stop="shut(index)"
            >
                <div>
                    <img :src="img.imgsrc" />
                </div>
            </div>
        </transition-group> */}
    </div>
        )
    }
}