import React from "react";
import './index.css'
import {Row,Col} from 'antd'
import img_left from '../../assets/hj3.jpg'
import img_right from '../../assets/hj4.jpg'

export default class GroupIntroduce extends React.Component {
    render() {
        return (
            <div id="teamIntruduce" className="teamIntruduce container">
                <div className="teamIntruduce-title">
                    <span className="bg-pic">团队介绍</span>
                </div>
                <div className="teamIntruduce-p">
                    <Col span="24">
                        <p>
                            工作室是由一群有梦想、热爱互联网的同学们自发成立，团队成员来自各个学院<br />
                        </p>

                        <p>
                            工作方式：
                            以研究助成长，以推广促提高，以技术及管理提升能力，在研发推广实用、有趣、智能的互联网应用的道路上勇往直前<br />
                        </p>
                        <p>
                            我们的奋斗目标：建设成一个有影响力、促进个人成长、能够持续发展的学生团队<br />
                        </p>
                    </Col>
                </div>

                <div className="teamIntruduce-photo">
                    <Row>
                        <Col xs="24" span="12">
                            <img src={img_left} alt="" />
                        </Col>
                        <Col xs="24" span="12">
                            <img src={img_right} alt="" />
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}