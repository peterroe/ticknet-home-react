import React from "react";
import './index.css'
export default class TeamIntroduce extends React.Component {
    render() {
        return (
            <div id="group" className="group container">
        <div className="group-title">
            <span>组别介绍</span>
        </div>
        <div className="group-contents">
            <div className="g-content">
                <div className="g-content-title">研发组</div>
                <div className="g-content-sidetitle">运维与安全工程师</div>
                <div className="g-content-p">进行日常的服务器运维</div>
                <div className="g-content-p">负责系统稳定和安全的工作</div>
                <div className="g-content-p">Linux、Web安全</div>

            </div>
            <div className="g-content">
                <div className="g-content-title">研发组</div>
                <div className="g-content-sidetitle">后端工程师</div>
                <div className="g-content-p">编写的代码在服务器端运行</div>
                <div className="g-content-p">为产品提供可靠的数据接口</div>
                <div className="g-content-p">PHP/Node/Java/Python</div>

            </div>

            <div className="g-content">
                <div className="g-content-title">研发组</div>
                <div className="g-content-sidetitle">前端工程师</div>
                <div className="g-content-p">负责PC和移动端网页开发</div>
                <div className="g-content-p">小程序、WebApp</div>
                <div className="g-content-p">JavaScript、CSS、HTML</div>

            </div>
            <div className="g-content">
                <div className="g-content-title">运营组</div>
                <div className="g-content-sidetitle">产品运营方向</div>
                <div className="g-content-p">负责工作室产品项目管理</div>
                <div className="g-content-p">完成用户调研、需求分析</div>
                <div className="g-content-p">Axure、墨刀、Xmind等</div>

            </div>
            <div className="g-content">
                <div className="g-content-title">运营组</div>
                <div className="g-content-sidetitle">新媒体编辑方向</div>
                <div className="g-content-p">湖南科技服务号运营</div>
                <div className="g-content-p">负责团队的外宣工作</div>
                <div className="g-content-p">Alexa、Spss、场景秀等</div>

            </div>
            <div className="g-content">
                <div className="g-content-title">策划组</div>
                <div className="g-content-sidetitle">策划方向</div>
                <div className="g-content-p">负责日常活动、招新策划</div>
                <div className="g-content-p">团队规章制度与监督</div>
                <div className="g-content-p">Word、Excel等</div>

            </div>
        </div>
    </div>
        )
    }
}