import React from "react";
import './index.css'
import {Row,Col} from 'antd'
import logo from '../../assets/logo4.jpg'
export default class Nav extends React.Component {
    render() {
        return (
            <div id="nav">
                <Row className="container">
                    <Col xs={24} sm={10} span={12} className="nav-left">
                        <div className="nav-left-title">
                            <img src={logo} className="logo"></img>
                            <span> TickNet Studio </span>
                        </div>
                    </Col>
                    <Col xs={0} sm={14} span={12} className="nav-right">
                        <div className="nav">
                            <ul>
                                <li><a href="#home">首页</a></li>
                                <li><a href="#product">产品介绍</a></li>
                                <li><a href="#group">组别介绍</a></li>
                                <li><a href="#member">团队成员</a></li>
                                <li><a href="#about">关于我们</a></li>
                            </ul>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}