import React from "react";
import './index.css'
import {Row,Col} from 'antd'
import img from '../../assets/QQcode.png'
export default class Address extends React.Component {
    render() {
        return (
            <div className="layer-foot">
            <div id="about" className="layer-foot-bottom">
                <Row className="container">
                    <Col xs="24" span="24">
                        <div className="bottom-mainTitle">Ticknet 工作室</div>
                        <div className="bottom-content">
                            地址：湖南科技大学南校逸夫楼308
                        </div>

                        <div className="bottom-content">
                            邮箱：2402007575@qq.com
                        </div>
                        <div className="bottom-icon">
                            <img src={img} alt="" />
                        </div>
                    </Col>
                </Row>
                
                    <div className="copyright">© 版权所有 湘ICP备16021626-1</div>
                
            </div>
        </div>
        )
    }
}