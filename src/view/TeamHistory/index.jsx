import React from "react";
import './index.css'
import { Timeline } from "antd";
export default class TeamHistory extends React.Component {
    render() {
        let activities = [
            {
                content: '工作室正式成立',
                timestamp: '2015-6',
                type: 'success',
                color: '#ade8f4'
            },
            {
                content: '挂靠网络信息中心，承担我校企业号开发',
                timestamp: '2016-1',
                color: '#90e0ef'
            },
            {
                content: '开始全面向全校范围招新，建立团队制度',
                timestamp: '2016-5',
                color: '#48cae4'
            },
            {
                content: '在老师的指导下，生产互联网产品10余项并上线',
                timestamp: '2018-5',
                color: '#00b4d8'
            },
            {
                content: '调整分组为：研发组设计组运营组',
                timestamp: '2018-4',
                color: '#0096c7'
            },
            {
                content: '设计组和运营组合并为运营组',
                timestamp: '2019-6',
                color: '#0077b6'
            },
            {
                content: '上线失物招领和校园闲置',
                timestamp: '2021-3',
                color: '#023e8a'
            }
        ]
        return (
            <div id="teamHistory" className="teamHistory container">
            <div className="teamHistory-title">团队历史</div>
            <div className="teamHistory-timeLine">
                <Timeline mode="alternate">
                    {
                        activities.map((item) => {
                            return (
                                <Timeline.Item
                                    label={item.timestamp}
                                    color={item.color}
                                    key={item.timestamp}
                                >
                                    {item.content}
                                </Timeline.Item>
                            )
                        })
                    }

                    
                </Timeline>
            </div>
        </div>
        )
    }
}