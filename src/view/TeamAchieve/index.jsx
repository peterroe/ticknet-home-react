import React from "react";
import './index.css'
import { Row,Col } from "antd";
import img from '../../assets/waityou.jpg'
export default class TeamAchieve extends React.Component {
    render() {
        return (
            <div id="achevement" className="achievement container">
            <Row>
                <Col sm={14} xs={24} >
                    <div className="achievement-title">团队成就</div>
                    <div className="achievement-p">
                        在湖南科技大学企业号上线了自主研发多个产品，如汽车入校，校园闲置，失物招领等，产品日访问量大。为解决学生校园生活日常需求问题提供了便利。科大企业号关注人数近八万人。
                    </div>
                    <div className="achievement-p">
                        软件著作权专利6项，团队获奖国家级比赛证书共3项，省级比赛证书2项，市级证书1项，校十佳科技创新团队
                    </div>
                    <div className="achievement-p">
                        团队许多老成员现就职于京东、哔哩哔哩、字节跳动等著名公司。
                    </div>
                </Col>
                <Col sm={10} xs={24} >
                    <div className="achievement-img">
                        <img src={img} alt="" />
                    </div>
                </Col>
            </Row>
        </div>
        )
    }
}