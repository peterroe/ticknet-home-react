import { Component } from 'react';
import './App.css';
import './animate.min.css'
import Nav from './view/Nav/index.jsx'
import Background from './view/BackGround/index.jsx'
import GroupIntroduce from './view/GroupIntroduce/index.jsx'
import TeamHistory from './view/TeamHistory';
import TeamAchieve from './view/TeamAchieve';
import ProductIntroduce from './view/ProductIntroduce';
import TeamIntroduce from './view/TeamIntroduce';
import MemberIntruduce from './view/MemberIntruduce';
import Address from './view/Address';
import img from './assets/rocket.png'
import {addClass,removeClass} from './util/oprateClass'
class App extends Component {
  componentDidMount() {
    const rocket = document.querySelector('.rocket')
        rocket.addEventListener('click', () => {
            let currentPosition = null
            let timer = null
            const speed = 30
            timer = setInterval(function () {
                currentPosition =
                    document.documentElement.scrollTop ||
                    document.body.scrollTop
                currentPosition -= speed // speed变量
                if (currentPosition > 0) {
                    window.scrollTo(0, currentPosition)
                } else {
                    window.scrollTo(0, 0)
                    clearInterval(timer)
                }
            }, 1)
        })
        function throttle(add, remove, rocket, delay) {
            let isRun = true
            return function (...args) {
                if (!isRun) {
                    return false
                }

                isRun = false

                setTimeout(() => {
                    if (window.pageYOffset > 600) {
                        remove(rocket, 'bounceOutDown')
                        add(rocket, 'bounceInUp')
                        isRun = true
                    } else {
                        remove(rocket, 'bounceInUp')
                        add(rocket, 'bounceOutDown')
                        isRun = true
                    }
                    isRun = true
                }, delay)
            }
        }
        window.onscroll = throttle(addClass, removeClass, rocket, 50)
  }
  render() {

    return (
      <div className="App">
        <div class="rocket animated">
            <img src={img} alt="" />
        </div>
        <Nav></Nav>
        <Background></Background>
        <GroupIntroduce></GroupIntroduce>
        <TeamHistory></TeamHistory>
        <TeamAchieve></TeamAchieve>
        <ProductIntroduce></ProductIntroduce>
        <div className="cutLine container"></div>
        <TeamIntroduce></TeamIntroduce>
        <MemberIntruduce></MemberIntruduce>
        <Address></Address>
      </div>
    );
  }
}

export default App;
